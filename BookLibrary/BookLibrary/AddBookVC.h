//
//  AddBookVC.h
//  BookLibrary
//
//  Created by Ivan on 10.05.16.
//  Copyright © 2016 Ivan. All rights reserved.
//

#import <UIKit/UIKit.h>
#include "Book.h"

@interface AddBookVC : UIViewController
-(void)setBookUI:(Book*) book;
@end
