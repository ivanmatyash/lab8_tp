//
//  AppDelegate.h
//  BookLibrary
//
//  Created by Ivan on 10.05.16.
//  Copyright © 2016 Ivan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import "Book.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

- (void)saveContext;
- (NSURL *)applicationDocumentsDirectory;
- (NSArray*)getAllBooks;
-(void)addNewBookWithName:(NSString*)bookName author:(NSString*)author yearPublishing:(NSString*)year numberOfPages:(NSNumber*) numberOfPages dateAdded:(NSDate*) date image:(NSData*)image readed:(NSNumber*)readed publishingHouse:(NSString*)publishingHouse rating:(NSNumber*)rating;
-(void)saveUpdate:(Book*)book name:(NSString*)name;
-(Book*)findByName:(NSString*)name;
-(NSArray*)sortByProperty:(NSString*)property asc:(BOOL) asc;
@end

