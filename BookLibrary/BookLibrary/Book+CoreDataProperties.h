//
//  Book+CoreDataProperties.h
//  BookLibrary
//
//  Created by Ivan on 10.05.16.
//  Copyright © 2016 Ivan. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Book.h"

NS_ASSUME_NONNULL_BEGIN

@interface Book (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *name;
@property (nullable, nonatomic, retain) NSString *author;
@property (nullable, nonatomic, retain) NSString *yearPublishing;
@property (nullable, nonatomic, retain) NSString *publishingHouse;
@property (nullable, nonatomic, retain) NSNumber *numberOfPages;
@property (nullable, nonatomic, retain) NSDate *dateAdded;
@property (nullable, nonatomic, retain) NSNumber *readed;
@property (nullable, nonatomic, retain) NSData *image;
@property (nullable, nonatomic, retain) NSNumber *rating;

@end

NS_ASSUME_NONNULL_END
