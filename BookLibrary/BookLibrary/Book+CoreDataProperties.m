//
//  Book+CoreDataProperties.m
//  BookLibrary
//
//  Created by Ivan on 10.05.16.
//  Copyright © 2016 Ivan. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Book+CoreDataProperties.h"

@implementation Book (CoreDataProperties)

@dynamic name;
@dynamic author;
@dynamic yearPublishing;
@dynamic publishingHouse;
@dynamic numberOfPages;
@dynamic dateAdded;
@dynamic readed;
@dynamic image;
@dynamic rating;

@end
