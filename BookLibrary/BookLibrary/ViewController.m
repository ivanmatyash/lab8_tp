//
//  ViewController.m
//  BookLibrary
//
//  Created by Ivan on 10.05.16.
//  Copyright © 2016 Ivan. All rights reserved.
//

#import "ViewController.h"
#include "Book.h"
#include "AppDelegate.h"
#include "AddBookVC.h"

@interface ViewController ()
@property (weak, nonatomic) IBOutlet UITextView *books;
@property AppDelegate *ad;
@property (weak, nonatomic) IBOutlet UITextField *bookNameUI;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    _books.editable = NO;
    //_bookNameUI.text = @"Martin Eden";
    _ad = [[AppDelegate alloc] init];
    [self showAll];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)addBook:(id)sender {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
    ViewController * vc = [storyboard instantiateViewControllerWithIdentifier:@"addBook"];
    [self presentViewController:vc animated:YES completion:nil];
}
- (IBAction)findByName:(id)sender {
    Book *book = [_ad findByName:_bookNameUI.text];
    if ( book != nil){
        [self showOnView:@[book]];
    }
    else{
        _books.text = @"There is no result";
    }
}

- (IBAction)showAllBooks:(id)sender {
    [self showAll];
}

-(void)showAll{
     _books.text = @"";
    NSArray *books = [_ad getAllBooks];
    if ( [books count] == 0){
        _books.text = @"There is no books in your library";
        return;
    }
    [self showOnView:books];
}

-(void)showOnView:(NSArray*)books{
    NSLog(@"showOnView");
    _books.text=@"";
    if ( [books count] == 0){
        _books.text = @"There is no result";
        return;
    }
    for(Book* book in books){
        NSLog(@"%@", book);
        NSString *text = _books.text;
        NSString *bookString = [NSString stringWithFormat:@"%@, %@, %@, %@\n", book.name, book.author, book.yearPublishing, book.rating];
        _books.text = [text stringByAppendingString:bookString];
    }

}
- (IBAction)sortByAuthor:(id)sender {
    NSArray *sorted = [_ad sortByProperty:@"author" asc:YES];
    [self showOnView:sorted];

}
- (IBAction)sortByYear:(id)sender {
    NSArray *sorted = [_ad sortByProperty:@"yearPublishing" asc:YES];
    [self showOnView:sorted];

}
- (IBAction)editBook:(id)sender {
    Book *book = [_ad findByName:_bookNameUI.text];
    if ( book != nil){
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
        AddBookVC * vc = [storyboard instantiateViewControllerWithIdentifier:@"addBook"];
        [self presentViewController:vc animated:YES completion:nil];
        [vc setBookUI:book];
    }
    else{
        _books.text = @"There is no result";
    }
}

@end
