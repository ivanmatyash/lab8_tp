//
//  Record+CoreDataProperties.m
//  TicketBooking
//
//  Created by Ivan on 8.05.16.
//  Copyright © 2016 Ivan. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Record+CoreDataProperties.h"

@implementation Record (CoreDataProperties)

@dynamic cityTo;
@dynamic cityFrom;
@dynamic aviaCompany;
@dynamic price;

@end
