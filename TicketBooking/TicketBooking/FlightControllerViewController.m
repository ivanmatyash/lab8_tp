//
//  FlightControllerViewController.m
//  TicketBooking
//
//  Created by Ivan on 8.05.16.
//  Copyright © 2016 Ivan. All rights reserved.
//

#import "FlightControllerViewController.h"
#import "ViewController.h"
#import "Record.h"
#import "AppDelegate.h"

@interface FlightControllerViewController ()
@property NSString* cityFrom;
@property NSString* cityTo;
@property (weak, nonatomic) IBOutlet UILabel *name;

@property (weak, nonatomic) IBOutlet UILabel *company;

@property (weak, nonatomic) IBOutlet UILabel *priceLabel;


@end

@implementation FlightControllerViewController

-(void)setCityFrom:(NSString *)cityFrom{
    _cityFrom = cityFrom;
}
-(void)setCityTo:(NSString *)cityTo{
    _cityTo = cityTo;
}
- (IBAction)backButton:(id)sender {
    NSString * storyboardName = @"Main";
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:storyboardName bundle: nil];
    ViewController * vc = [storyboard instantiateViewControllerWithIdentifier:@"main"];
    [self presentViewController:vc animated:YES completion:nil];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    _name.text = [NSString stringWithFormat: @"From %@ to %@", self.cityFrom, self.cityTo];
    [self initLabels];
    // Do any additional setup after loading the view.
}
-(void)initLabels{
    NSString *companyName;
    double price = -1;
    AppDelegate *ad = [UIApplication sharedApplication].delegate;
    NSArray *flights = [ad getAllFlights];
    NSLog(@"%lu", [flights count]);
    for(Record *rec in flights){
        //NSLog(@"%@", rec.cityFrom);
        if ( [rec.cityTo isEqualToString:self.cityTo] && [rec.cityFrom isEqualToString:self.cityFrom]){
            if ( companyName == nil){
                companyName = rec.aviaCompany;
                price = [rec.price doubleValue];
                continue;
            }   
            else{
                if ( price > [rec.price doubleValue]){
                    price = [rec.price doubleValue];
                    companyName = rec.aviaCompany;
                }
            }
        }
    }
    if ( companyName != nil){
       // NSLog(@"%@", companyName);
        _company.text = companyName;
        _priceLabel.text = [NSString stringWithFormat:@"%lf", price];
    }
    else{
        _company.text = @"Empty";
        _priceLabel.text = @"NULL";
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
