//
//  ViewController.m
//  TicketBooking
//
//  Created by Ivan on 8.05.16.
//  Copyright © 2016 Ivan. All rights reserved.
//
#import "ViewController.h"
#import <MapKit/MapKit.h>
#import "FlightControllerViewController.h"

@interface ViewController (){
    int isCity;
    MKPointAnnotation *annotatiionFrom;
    MKPointAnnotation *annotatiionTo;
}
@property (weak, nonatomic) IBOutlet MKMapView *map;
@property (weak, nonatomic) IBOutlet UITextField *cityFrom;
@property (weak, nonatomic) IBOutlet UITextField *cityTo;
@property (weak, nonatomic) IBOutlet UIButton *find;

@end

@implementation ViewController
- (IBAction)textField2:(id)sender {
    isCity = 1;
}
- (IBAction)textField1:(id)sender {
    isCity = 0;
}
- (IBAction)replace:(id)sender {
    NSString* temp = _cityTo.text;
    _cityTo.text = _cityFrom.text;
    _cityFrom.text = temp;
    
}

- (IBAction)showFlights:(id)sender {
    NSString * storyboardName = @"Main";
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:storyboardName bundle: nil];
    FlightControllerViewController * vc = [storyboard instantiateViewControllerWithIdentifier:@"flight"];
    [(FlightControllerViewController*)vc setCityFrom: _cityFrom.text];
    [(FlightControllerViewController*)vc setCityTo:_cityTo.text];
    [self presentViewController:vc animated:YES completion:nil];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.

    UILongPressGestureRecognizer *longPressGesture =[[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(handleLongPressGesture:)];
    [self.map addGestureRecognizer:longPressGesture];
}

-(void)handleLongPressGesture:(UIGestureRecognizer*)sender

{ if (sender.state == UIGestureRecognizerStateEnded)
    
{
    NSLog(@"handleLongPressGesture");
    CGPoint point = [sender locationInView:self.map];
    CLGeocoder *geocoder = [[CLGeocoder alloc] init];
    CLLocationCoordinate2D coord = [self.map convertPoint:point toCoordinateFromView:self.map];
    CLLocation *location = [[CLLocation alloc] initWithLatitude:coord.latitude longitude:coord.longitude];
    [geocoder reverseGeocodeLocation:location completionHandler:^(NSArray *placemarks, NSError *error) {
        if (error){
            NSLog(@"Geocode failed with error: %@", error);
            return;
        }
        for (CLPlacemark * placemark in placemarks){
            NSLog(@"isCity = %d", isCity);
            [self setAnnotationToMap:isCity :placemark.locality :coord];
        };
    }];
}
}

-(void)setAnnotationToMap:(int)type :(NSString *)title :(CLLocationCoordinate2D)coordinate {
    NSLog(@"setAnnotationToMap");
    if (type == 0) {
        [_map removeAnnotation:annotatiionFrom];
        annotatiionFrom= [[MKPointAnnotation alloc] init];
        annotatiionFrom.title = title;
        annotatiionFrom.coordinate = coordinate;
        [_map addAnnotation:annotatiionFrom];
        self.cityFrom.text = title;
    }
    else {
        [_map removeAnnotation:annotatiionTo];
        annotatiionTo= [[MKPointAnnotation alloc] init];
        annotatiionTo.title = title;
        annotatiionTo.coordinate = coordinate;
        [_map addAnnotation:annotatiionTo];
        self.cityTo.text = title;
    }
}

-(void)textFieldDidBeginEditing:(UITextField *)textField {
    if (textField == self.cityFrom)
        isCity = 0;
    else if (textField == self.cityTo)
        isCity = 1;
    [textField resignFirstResponder];
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
